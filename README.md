## Portfólio - Frontend Developer
Bem-vindo(a) ao meu portfólio! Sou um Desenvolvedora Frontend apaixonada por design e interfaces atraentes e funcionais.

Olá, bem-vindo(a) ao meu portifílo! Sou Dev Frontend e a cada dia gosto mais dessa área. 
Este portfólio é uma forma de compartilhar um pouco dos meus trabalhos.

### Sobre Mim
Busco melhorar sempre e ir atrás das novidades para me manter atualizada sobre as tendências de tecnologia e afins. Sou determinada e gosto de novos desafios, tenho também facilidade de aprender novos assuntos.

### Habilidades
Aqui estão algumas das minhas habilidades técnicas:

HTML5, CSS3 e JavaScript
Design responsivo
Git e controle de versão

### Contato
Entre em contato:

[E-mail](emillpatricia.d@hotmail.com)  
[LinkedIn](linkedin.com/in/emille-ferreira)  
